var express = require('express');
var router = express.Router();
var account_dal = require('../model/account_dal');
var school_dal = require('../model/school_dal');
var company_dal = require('../model/company_dal');
var skill_dal = require('../model/skill_dal');


// View All accounts
router.get('/all', function(req, res) {
    account_dal.getAll(function(err, result){
        if(err) {
            res.send(err);
        }
        else {
            res.render('account/accountViewAll', { 'result':result });
        }
    });

});

// View the company for the given id
router.get('/', function(req, res){
    if(req.query.account_id == null) {
        res.send('account_id is null');
    }
    else {
        account_dal.getById(req.query.account_id, function(err,result) {
          if (err) {
              res.send(err);
              }
          else {
              res.render('account/accountViewById', {'account': result[0][0], 'school': result[1], 'skill': result[2], 'company': result[3]});
              }
        });
    }
});

// Return the add a new account form
router.get('/add', function(req, res){
    // passing all the query parameters (req.query) to the insert function instead of each individually
    school_dal.getAll(function(err,result) {
        company_dal.getAll(function (err, result2) {
            skill_dal.getAll(function (err, result3) {
                if (err) {
                    res.send(err);
                }
                else {
                    res.render('account/accountAdd', {'school': result, 'company': result2, 'skill': result3});
                }
            })
        })
    });
});


// View the account for the given id
router.get('/insert', function(req, res) {
    // simple validation
    if (req.query.first_name == null) {
        res.send('First Name must be provided.');
    }
    else if (req.query.last_name == null) {
        res.send('Last Name must be provided.');
    }
    else if (req.query.email == null) {
        res.send('Email must be provided.');
    }
    else {
        // passing all the query parameters (req.query) to the insert function instead of each individually
        account_dal.insert(req.query, function (err, result) {
            if (err) {
                console.log(err);
                res.send(err);
                }
                else {
                //poor practice for redirecting the user to a different page, but we will handle it differently once we start using Ajax
                res.redirect(302, '/account/all');
                }
        });
    }
});



router.get('/edit', function(req, res){
    if(req.query.account_id == null) {
        res.send('An account id is required');
    }
    else {
        account_dal.edit(req.query.account_id, function(err, result){
            res.render('account/accountUpdate', {account: result[0][0], school: result[1], skill: result[2], company: result[3]});
        });
    }

});

/*
router.get('/edit2', function(req, res){
    if(req.query.account_id == null) {
        res.send('An account id is required');
    }
    else {
        account_dal.getById(req.query.account_id, function(err, account) {
            school_dal.getAll(function (err, school) {
                skill_dal.getAll(function (err, skill) {
                    company_dal.getAll(function (err, company) {
                        res.render('account/accountUpdate', {
                            account: account, school: school, skill: skill, company: company  });
                    });
                });
            });
        });
    }
});
*/

router.get('/update', function(req, res) {
    account_dal.update(req.query, function(err, result){
        account_dal.updateSkill(req.query, function(err, result){
            account_dal.updateCompany(req.query, function(err, result){
                res.redirect(302, '/account/all');
            });
        });
    });
});

// Delete an account for the given account_id
router.get('/delete', function(req, res){
    if(req.query.account_id == null) {
        res.send('account_id is null');
    }
    else {
        account_dal.delete(req.query.account_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else {
                //poor practice, but we will handle it differently once we start using Ajax
                res.redirect(302, '/account/all');
            }
        });
    }
});

module.exports = router;
